import axios from 'axios';


async function fetchDocFromId(docId) {
  return (await axios.get(
    `/.netlify/functions/getDocument?docId=${docId}`)).data;
}

async function fetchDocFromEditKey(editKey) {
  return (await axios.get(
    `/.netlify/functions/getDocument?docEditKey=${editKey}`)).data;
}

export {
  fetchDocFromId, fetchDocFromEditKey
};
