import React, { useState, useEffect } from 'react';
import {useParams, withRouter} from 'react-router-dom';
import bml from 'bml';
import { fetchDocFromId } from './data.js';

function Document(props) {
  const [bmlDoc, setBmlDoc] = useState(null);
  const { docId } = useParams();

  useEffect(() => {
    async function fetchDoc() {
      try {
        setBmlDoc((await fetchDocFromId(docId)).content);
      } catch (e) {
        console.error(e);
      }
    }
    fetchDoc();
  }, [docId]);
  
  let renderedResult = '';
  if (bmlDoc) {
    try {
      renderedResult = bml(bmlDoc, null, {renderMarkdown: true});
    } catch (e) {
      console.warn(e);
    }
  }

  return (
      <div className="Document">
        <div className="DocumentBody" dangerouslySetInnerHTML={{__html: renderedResult}}>
        </div>
      </div>
  );
}

export default withRouter(Document);
