import React from 'react';
import './modal.css';

function LoadingModal() {
  return (
    <div className="modal">
      <section className="modal-main">
        loading...
      </section>
    </div>
  );
}

export default LoadingModal;
