import React from 'react';
import {Link, withRouter} from 'react-router-dom';

function ShareInfo(props) {
  return (
    <div className="ShareInfo">
      <p>
      {props.editedSinceSave ?
       'You have unsaved changes; the links below will point to the previously saved code until you click "Save Changes" above'
       : 'Successfully saved.'
      }
      </p>
      <p>
      Share this code with <Link to={`?from=${props.docId}`}>this link</Link>. Share a rendered-only view of this code with <Link to={`/doc/${props.docId}`}>this link</Link>.
      </p>
      <p>
      You can continue editing the code on this page and save again to modify it. If you want to come back later and edit the saved code, you can use <Link to={`?edit=${props.editKey}`}>this secret link</Link>. Keep this link safe and secret if you want to control access to the document!
      </p>
    </div>
  );
}

export default withRouter(ShareInfo);
