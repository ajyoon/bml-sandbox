import React from 'react';

function CheatSheet() {
  return (
    <div className="CheatSheet">
      <h3>Cheat Sheet</h3>
      <ul>
        <li>Inline choice: <code>{"{(X), (Y)}"}</code> → <i>X</i> or <i>Y</i></li> 
        <li>With weights: <code>{"{(X) 70, (Y)}"}</code> → <i>X</i> (70% of the time) or <i>Y</i> (30% of the time)</li> 
        <li>Nested choice: <code>{"{(X), (nested {(Y), (Z)})}"}</code> → <i>X</i> or (<i>nested Y</i> or <i>nested Z</i>)<br/>(Syntax highlighting doesn't play nice with nested choices, sorry)</li>
        <li>References: <code>{"{Name: (Alice), (Bob)} tied {@Name: 0 -> (her), 1 -> (his)} shoe."}</code></li> 
      </ul>
      <p>
        See <a href="https://bml-lang.org">bml-lang.org</a> for full documentation.
      </p>
    </div>
  );
}

export default CheatSheet;
