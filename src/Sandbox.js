import React, { useState, useEffect } from 'react';
import Editor from 'react-simple-code-editor';
import {withRouter, useLocation} from "react-router-dom";
import './Sandbox.css';
import CheatSheet from './CheatSheet.js';
import ShareInfo from './ShareInfo.js';
import LoadingModal from './LoadingModal.js';
import bml from 'bml';
import axios from 'axios';
import {fetchDocFromId, fetchDocFromEditKey} from './data.js';

/**
 * returns {docId, editKey}
 */
async function save(content, editKey) {
  const result = await axios.post(`/.netlify/functions/putDocument`, {
    editKey, content
  });
  return { docId: result.data.docId, editKey: result.data.editKey };
}

function renderBml(source) {
  try {
    return bml(source, null, {renderMarkdown: true});
  } catch (e) {
    return e;
  }
}

function formatError(error) {
  return (
    <div className="bmlError">
    {error &&
     error.name + ': ' + error.message}
    </div>
  );
}

const initialCode = `Hello, {(bml), (BML)} sandbox!
`;

function Sandbox({history, location}) {
  const [code, setCode] = useState(initialCode);
  const [bmlError, setBmlError] = useState(null);
  const [lastValidPreviewHtml, setLastValidPreviewHtml] = useState(renderBml(code));
  const [docId, setDocId] = useState(null);
  const [editKey, setEditKey] = useState(null);
  const [editedSinceSave, setEditedSinceSave] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const query = new URLSearchParams(useLocation().search);
  
  useEffect(() => {
    (async () => {
      try {
        const queryDocId = query.get("from");
        const queryEditKey = query.get("edit");
        if (queryDocId) {
          setLoading(true);
          setCode((await fetchDocFromId(queryDocId)).content);
        } else if (queryEditKey) {
          setLoading(true);
          let doc = await fetchDocFromEditKey(queryEditKey);
          setCode(doc.content);
          setDocId(doc.docId);
          setEditKey(queryEditKey);
          setEditedSinceSave(false);
          history.replace({...location, search: null});
        }
        if (queryDocId || queryEditKey) {
          setLoading(false);
        }
      } catch (e) {
        console.error(e);
      }
    })();
    // eslint-disable-next-line
  }, [history, location]);
  
  function handleBmlChange(code) {
    if (!editedSinceSave && editKey != null) {
      setEditedSinceSave(true);
    }
    setCode(code);
    let bmlResult = renderBml(code, setBmlError);
    if (typeof bmlResult === 'string' || bmlResult instanceof String) {
      setLastValidPreviewHtml(bmlResult);
      setBmlError(null);
    } else {
      setBmlError(bmlResult);
    }
  }
  
  async function handleSave() {
    try {
      const {docId: newDocId, editKey: newEditKey}
            = await save(code, editKey);
      setEditedSinceSave(false);
      setDocId(newDocId);
      setEditKey(newEditKey);
    } catch (e) {
      console.error('save failed', e);
      window.alert('save failed, maybe try again?');
    }
  }

  return (
    <div className="Sandbox">
      <h2>Live Editor</h2>
      <CheatSheet/>
      <i>Please note that this sandbox tool is under active development, so expect bugs! There's also a chance we might delete the saved documents at some point if we need to make breaking changes, so don't count on that functionality yet...</i>
      <hr/>
      <div className="interactive-editor">
        <div className="editor-label">editor:</div>
        <Editor
          className="editor"
          autoFocus
          value={code}
          onValueChange={handleBmlChange}
          padding={'1em'}
          highlight={window.bmlHighlighting.highlightBml}
          style={{
          }}
        />
        <div className="preview-label">preview:</div>
        <div className="preview">
          <div className="preview-text" dangerouslySetInnerHTML={{__html: lastValidPreviewHtml}}></div>     
        </div>
        {formatError(bmlError)}
        <button className="rerender-button" onClick={(e) => handleBmlChange(code)}
          >Re-render</button>
        <button className="save-button" onClick={handleSave}
          >{editedSinceSave ? 'Save Changes' : 'Save'}</button>
      </div>
      {editKey &&
        <ShareInfo docId={docId} editKey={editKey} editedSinceSave={editedSinceSave}/>
      }
      {isLoading && <LoadingModal/>}
    </div>
  );
}

export default withRouter(Sandbox);
