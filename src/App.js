import React from 'react';
import './App.css';
import Sandbox from './Sandbox.js';
import Document from './Document.js';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/doc/:docId">
            <Document/>
          </Route>
          <Route path="/">
            <Sandbox/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
