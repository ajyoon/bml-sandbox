const faunadb = require('faunadb'),
      q = faunadb.query;

const { FAUNA_KEY_SECRET } = process.env;
const client = new faunadb.Client({ secret: FAUNA_KEY_SECRET });

const lookupDoc = async (indexName, key) => {
  return await client.query(
    q.Get(
      q.Match(q.Index(indexName), key)
    )
  );
};

exports.lookupDoc = lookupDoc;

exports.handler = async (event, context) => {
  const { docId, docEditKey } = event.queryStringParameters;
  
  let lookupResult;
  try {
    let indexName;
    let indexKey;
    if (docId) {
      indexName = 'documentId';
      indexKey = docId;
    } else if (docEditKey) {
      indexName = 'documentEditKey';
      indexKey = docEditKey;
    } else {
      return { statusCode: 400, body: 'No docId or docEditKey provided' };
    }
    lookupResult = await lookupDoc(indexName, indexKey);
  } catch(e) {
    if (e.requestResult.statusCode === 404) {
      return {
        statusCode: 404,
        body: "Document not found"
      };
    } else {
      console.error(e);
      return {
        statusCode: 500,
        body: "Unexpected lookup error"
      };
    }
  }

  return {
    statusCode: 200,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      docId: lookupResult.data.id,
      content: lookupResult.data.content
    })
  };
};
