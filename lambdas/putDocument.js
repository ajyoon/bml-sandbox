const crypto = require('crypto');
const base64url = require('base64url');
const faunadb = require('faunadb'),
      q = faunadb.query;
const lookupDoc = require('./getDocument.js').lookupDoc;

const { FAUNA_KEY_SECRET } = process.env;
const client = new faunadb.Client({ secret: FAUNA_KEY_SECRET });

function createDocId() {
  return base64url(crypto.randomBytes(10));
}

function createEditKey() {
  return base64url(crypto.randomBytes(25));
}

const getExistingDocFromEditKey = async (editKey) => {
  try {
    return await lookupDoc('documentEditKey', editKey);
  } catch (e) {
    if (e.name !== 'NotFound') {
      console.warn('Unexpected error when looking up document edit key: ' + JSON.stringify(e));
    }
    return null;
  }
};

exports.handler = async (event, context) => {
  const { editKey: providedEditKey, content } = JSON.parse(event.body);
  
  let refId;
  let docId;
  let editKey;

  try {
    let existingDocLookupResult = providedEditKey ?
        await getExistingDocFromEditKey(providedEditKey) : null;

    if (existingDocLookupResult) {
      refId = existingDocLookupResult.ref.id;
      docId = existingDocLookupResult.data.id;
      editKey = existingDocLookupResult.data.editKey;
    } else {
      refId = q.NewId();
      docId = createDocId();
      editKey = createEditKey();
    }

    await client.query(
      q.Insert(
        q.Ref(q.Collection('documents'), refId),
        q.Now(),
        'create',
        {data: {id: docId, editKey: editKey, content: content}}
      )
    );
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      body: "Unexpected write error"
    };
  }
  return {
    statusCode: 200,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({docId, editKey})
  };
};
